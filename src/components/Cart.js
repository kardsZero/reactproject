import React from 'react'
import './cart.css'
function Cart({cartItem,cart,setCart}) {
  const handleDecrease=()=>{
    console.log(handleDecrease);
     if(cartItem.quantity !== 1){
        const updateCart2 = cart.map((item)=>
          item.id===cartItem.id?{...item,quantity:item.quantity - 1 }: item
        )
        setCart(updateCart2)
     }
  }
  const handleIncrease=()=>{
    const updateCart = cart.map((item)=>
      item.id===cartItem.id?{...item,quantity:item.quantity + 1}:item
    )
      setCart(updateCart)
  }
  const handleClear=()=>{
    const updateCartAfterDeleteItem = cart.filter((item)=>item.id!==cartItem.id);//using filter to delete
    setCart(updateCartAfterDeleteItem)
  }
  return (

    <div>
      <table>
        <thead>
          <th>Name</th>
          <th>Price</th>
          <th>Quantity</th>

        </thead>
             <tbody key={cartItem.id} className='cartData'>
              <td> {cartItem.name}</td>
              <td> Rs.{cartItem.price} </td>
              <td>{cartItem.quantity}</td>
              <td><button onClick={handleDecrease} className='btn minus'>-</button></td>
              <td><button onClick={handleClear} className='btn clear'>Clear Cart</button></td>
              <td><button onClick={handleIncrease} className='btn plus'>+</button></td>
            </tbody>   
     </table>
    </div>

  )
}

export default Cart
