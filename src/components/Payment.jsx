import { useState } from "react";
import "./payment.css"
const Payment = ({isOpen,onClose,getTotal,setCart,setIsOpen}) =>{
    const [pay,setPay]= useState(false)
    if (!isOpen)
    return null;

   const handleproceed =()=>{
    setPay(true)

   };
   const handlepaid =()=>{
    alert('Thank you..Your payment is successfully Paid')
    setIsOpen(false)
    setCart([])
   }

    return (
       
            <div className="modal">
            <p>Are you sure you want to proceed with the Payment of Rs.{getTotal}</p>
            <button onClick={onClose} className="btn cancle">Cancle</button>
            <button onClick={handleproceed} className="btn proceed">Proceed</button>
            {
            pay && (
                <div>
                <p>Press the Button below to finish your payment</p>
                    <button onClick={handlepaid} className="btn paid">Paid</button>
                </div>
            )
        }
        </div>
      
    )
}
export default Payment