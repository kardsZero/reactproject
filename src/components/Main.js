import React, { useEffect, useState } from 'react';
import "./products.css";
import AllProducts from './AllProduct';
import Cart from './Cart';
import Payment from './Payment';

function Main() {
  const [cart, setCart] = useState([]);
  const [isOpen,setIsOpen]= useState(false)

  const handleAddtoCard = (item) => {
    
    //we can add just this to list down all the item on the cart one by one after clicked add to card button
    // setCart([...cart,item])

    //do this if we do not want to list down all the item in the cart but just increase the quantity of the item in the cart
    const itemIndex = cart.findIndex((cartIndex)=>cartIndex.id===item.id);
    if(itemIndex !== -1){
         // If the item is already in the cart, update its quantity
         const updateCart = [...cart]
         updateCart[itemIndex].quantity += 1;
         setCart(updateCart)
    }else{
      // If the item is not in the cart, add it with a quantity of 1
    setCart([...cart, {...item,quantity:1}]);
    }
  };

  useEffect(()=>{
    console.log("cart Has Changed",cart);
  },[cart])
    //calculate total price
    const getTotal=()=>{
      return cart.reduce((total,cartItem)=>total + cartItem.price*cartItem.quantity,0)//the 0 is required as initial value to ensure that the reduction start from 0
    }

    //handleProceedToPay
    const handleProceedToPay =()=>{
      setIsOpen(true)
    }
    const handleClose=()=>{
      setIsOpen(false)
    }

  return (
    <div className='wrapper'>
      <h1 className='header'>Kards Watch Store</h1>
      <div className='products-container'>
          {
            AllProducts.map((items) =>
              <div className='products' key={items.id}>
                <div><img src={items.image}/></div>
                <p>Name: {items.name}</p><p> Price: Rs.{items.price}</p>
                <button onClick={() => handleAddtoCard(items) } className='button'>Add to cart</button>
                {/* if we do not give the () => in the onclick this error will appear react-dom.development.js:26923 Uncaught Error: 
                Too many re-renders. React limits the number of renders to prevent an infinite loop...etc */}
              </div>
            )
          }
      </div>
        <>
          <div>
              <h2>Shopping Cart</h2>
              <div>
                  {cart.map((cartItem) => (
                    <Cart cartItem={cartItem} key={cartItem.id} cart={cart} setCart={setCart} />
                  ))}
              </div>
                     {cart.length > 0 &&(
                        <div>
                            <p>Your total is Rs.{getTotal()} </p>
                            <button onClick={handleProceedToPay} className='btn Proceed-to-pay'>Proceed To pay</button>
                        </div>
              )}
          </div>  
          <Payment onClose={handleClose} isOpen={isOpen} getTotal={getTotal()} setCart={setCart} setIsOpen={setIsOpen}/>
        </>
       
    </div>
    //we can just pass the props there or we can just do it here also
    // {cart.map((cartItem) => (
    //     <li key={cartItem.id}>
    //       {/* Display the name and price of the cartItem */}
    //       Name: {cartItem.name} - Price: Rs.{cartItem.price}
    //     </li>
    //   ))}
  )
}

export default Main;
