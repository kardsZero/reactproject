import Ap from "../../src/images/Audemars-Piguet.png"
import Olevs from "../../src/images/olevs.png"
import Hublot from "../../src/images/hublot.png"
import Rolex from "../../src/images/rolex.png"

const AllProducts = [{
    id:1,
    name:"Audemars Piguet",
    price:"4109072",
    image: Ap
},
{
    id:2,
    name:" Olevs ",
    price:'5000',
    image:Olevs
},{
    id:3,
    name:"Hublot",
    price:'697500',
    image:Hublot
},{
    id:4,
    name:"Rolex",
    price:"6582816",
    image:Rolex
},

]
export default AllProducts